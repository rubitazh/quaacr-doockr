/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.remote.tcp;

import org.glassfish.tyrus.client.ClientManager;
import org.punch.remote.utils.Utils;

import javax.websocket.*;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

@ClientEndpoint
public class RemoteWsEndpoint implements WsEndpoint {

    private static final Logger logger = Logger.getLogger(RemoteWsEndpoint.class.getName());

    private final ProxyServer proxy;
    private final String wsUrl;
    private final String id;

    private Session session;

    public RemoteWsEndpoint(ProxyServer proxy, String wsUrl) {
        this.proxy = proxy;
        this.wsUrl = wsUrl;
        this.id = Utils.getId(wsUrl);
    }

    @Override
    public void connect() throws Exception {
        var manager = ClientManager.createClient();
        var uri = new URI(wsUrl);
        session = manager.connectToServer(this, uri);
        proxy.registerEndpoint(id, this);

        logger.log(Level.INFO, "Connected to WS. [url={0}]", wsUrl);
    }

    @Override
    public void sendMsg(String req) {
        session.getAsyncRemote().sendText(req);
    }

    @OnOpen
    public void opOpen(Session session) {
        //session opened
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) {
        logger.log(Level.INFO, "WS connection closed. [id={0}]", id);
    }

    @OnError
    public void onError(Session session, Throwable e) {
        logger.log(Level.SEVERE, "Unexpected session error. [id=" + id + "]", e);
    }

    @OnMessage
    public void onMessage(String msg) {
        try {
            proxy.accept(id, msg);

        } catch (EndpointException e) {
            throw new RuntimeException("Accept msg error.", e);
        }
    }
}