/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.remote.http;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FileUtils {

    private static final Logger logger = Logger.getLogger(FileUtils.class.getName());

    static void deleteDirectory(Path path) {
        try {
            Files.walkFileTree(path, fileVisitor());

        } catch (IOException e) {
            logger.log(Level.WARNING, "Cannot delete dir, maybe some bad carma involved. Lets try one more time. " +
                    "[path={0}]", path);
            //Try one more time:
            try {
                Files.walkFileTree(path, fileVisitor());

                logger.info("All profile dirs deleted.");

            } catch (NoSuchFileException ex) {
                logger.info("Seems like file already been deleted. [path=" + e.getMessage() + "]");

            } catch (IOException ex) {
                throw new UncheckedIOException(ex);
            }
        }
    }

    private static FileVisitor<? super Path> fileVisitor() {
        return new SimpleFileVisitor<>() {

            @Override
            public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                Files.delete(dir);

                if (Objects.nonNull(exc)) {
                    throw exc;
                }

                return FileVisitResult.CONTINUE;
            }

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                Files.delete(file);
                return FileVisitResult.CONTINUE;
            }
        };
    }
}
