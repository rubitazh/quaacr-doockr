/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.remote.utils;

public class Utils {

    public static String getId(String url) {
        return url.substring(url.lastIndexOf("/") + 1);
    }
}
