FROM ubuntu:bionic-20191029

RUN apt-get -qqy update \
  && apt-get -qqy --no-install-recommends install \
    bzip2 \
    ca-certificates \
    openjdk-11-jre \
    maven \
    tzdata \
    sudo \
    unzip \
    wget \
    gnupg2 \
    curl \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# Timezone
ENV TZ "UTC"
RUN echo "${TZ}" > /etc/timezone \
  && dpkg-reconfigure --frontend noninteractive tzdata

# chrome user
RUN useradd chrome \
         --shell /bin/bash  \
         --create-home \
  && usermod -a -G sudo chrome \
  && echo 'ALL ALL = (ALL) NOPASSWD: ALL' >> /etc/sudoers \
  && echo 'chrome:secret' | chpasswd

ENV HOME=/home/chrome

# Xorg envs
ENV SCREEN_WIDTH 1440
ENV SCREEN_HEIGHT 900
ENV SCREEN_DEPTH 24
ENV SCREEN_DPI 96
ENV DISPLAY :99.0

# Xvfb
RUN apt-get update -qqy \
  && apt-get -qqy install \
    xvfb \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# Directory for Xvfb
RUN  mkdir -p /tmp/.X11-unix && sudo chmod 1777 /tmp/.X11-unix

# Locale
ENV LANG_WHICH en
ENV LANG_WHERE US
ENV ENCODING UTF-8
ENV LANGUAGE ${LANG_WHICH}_${LANG_WHERE}.${ENCODING}
ENV LANG ${LANGUAGE}

RUN apt-get -qqy update \
  && apt-get -qqy --no-install-recommends install \
    language-pack-en \
    locales \
  && locale-gen ${LANGUAGE} \
  && dpkg-reconfigure --frontend noninteractive locales \
  && apt-get -qyy autoremove \
  && rm -rf /var/lib/apt/lists/* \
  && apt-get -qyy clean

# Fonts
RUN apt-get -qqy update \
  && apt-get -qqy --no-install-recommends install \
    libfontconfig \
    libfreetype6 \
    xfonts-cyrillic \
    xfonts-scalable \
    fonts-liberation \
    fonts-ipafont-gothic \
    fonts-wqy-zenhei \
    fonts-tlwg-loma-otf \
    ttf-ubuntu-font-family \
  && rm -rf /var/lib/apt/lists/* \
  && apt-get -qyy clean

# fluxbox
RUN apt-get update -qqy \
  && apt-get -qqy install \
    fluxbox \
   && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# wnctrl
RUN apt-get -qqy update \
      && apt-get -qqy --no-install-recommends install \
        wmctrl \
      && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# x11vnc
ENV START_VNC_SERVER false

RUN apt-get update -qqy \
  && apt-get -qqy install \
  x11vnc \
  && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

# Chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list \
    && apt-get update -qqy \
    && apt-get -qqy install \
        google-chrome-stable \
        pulseaudio \
    && rm /etc/apt/sources.list.d/google-chrome.list \
    && rm -rf /var/lib/apt/lists/* /var/cache/apt/*

COPY chrome-docker-bootstrap.sh /opt

USER chrome

ENV PATH_TO_EXEC /usr/bin/google-chrome
ENV PROFILE_PATH "${HOME}"/quaacr_profiles
ENV AWAIT_CONN_TIMEOUT 5000
ENV PROXY_PORT 9999
ENV HTTP_PORT 5141
ENV N_THREADS 16
ENV DBUS_SESSION_BUS_ADDRESS=/dev/null

RUN mkdir "${HOME}"/quaacr-tcp-proxy
COPY quaacr-tcp-proxy "${HOME}"/quaacr-tcp-proxy

RUN cd "${HOME}"/quaacr-tcp-proxy \
    && mvn clean package

USER root

EXPOSE 5141 5900 9999

CMD ["./opt/chrome-docker-bootstrap.sh"]