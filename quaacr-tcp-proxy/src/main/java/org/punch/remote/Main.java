/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.remote;

import org.punch.remote.http.ChromeSessionController;
import org.punch.remote.tcp.ProxyServer;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    public static void main(String[] args) {
        System.setProperty("java.util.logging.SimpleFormatter.format", "%4$s: %5$s%n");

        int proxyPort = Integer.parseInt(System.getenv("PROXY_PORT"));
        int httpPort = Integer.parseInt(System.getenv("HTTP_PORT"));
        int nThreads = Integer.parseInt(System.getenv("N_THREADS"));

        final ExecutorService exec = Executors.newFixedThreadPool(nThreads);
        final ProxyServer proxy = new ProxyServer(proxyPort, exec);
        final ChromeSessionController controller = new ChromeSessionController(httpPort, proxy, exec);
        proxy.registerController(controller);

        //Start proxy:
        exec.execute(() -> {
            try {
                proxy.start();

            } catch (IOException e) {
                exec.shutdown();
                throw new RuntimeException("TCP proxy cannot be started.", e);
            }
        });

        //Start http server:
        exec.execute(() -> {
            try {
                controller.start();

            } catch (IOException e) {
                exec.shutdown();
                throw new RuntimeException("Http Server cannot be started.", e);
            }
        });
    }
}
