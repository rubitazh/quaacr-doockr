# Quaacr-doockr #

Hi. This is a part of *quaacr* lib.

What is *quaacr?* This is Java lib, which allows you do Web UI test automation for a Blink engine based browsers 
(Google Chrome, Chromium, Microsoft Edge).

It uses Chrome Devtools protocol, so, it's a kind of reliable. I mean, at least it provides feedback.

It can be run on Java 11 or higher. Yes, I know, Java 8 would be cool, but sorry, I'm kind of dumb, and you will figure out why in a minute.

I'm a QA (now you know), so I wrote this tool as a QA and for QA. What does it mean? It means, that this tool is simple and it works. Sarcasm.
Well, it works actually, but it's alpha, so, just keep that in mind.

For more information about quaacr lib visit this page: [quaacr magic](https://bitbucket.org/rubitazh/quaacr-client).

This part if for running Chrome in Docker container. You can run it as fully-featured Chrome, you can view what actually happens there,
if you need (no headless only mode, real web UI - isn't it nice?), and of course you can do this headless way.

### Setup quaacr-doockr ###

If you need this thing on your machine, follow the steps below:

* clone this repo
* from project root directory build Docker image: 

    `docker build -t jawisp/quaacr-doockr-chrome-93:0.0.17 .`


* start container (actually, you can do this without previous step - it will pull right from Docker hub, but building image is always fun, 
so who am I to deprive you of this opportunity): 

    `docker run -p 5141:5141 -p 9999:9999 -v /dev/shm:/dev/shm --user chrome --privileged jawisp/quaacr-doockr-chrome-93:0.0.17`

    
* if you need this with VNC:

    `docker run -p 5141:5141 -p 5900:5900 -p 9999:9999 -v /dev/shm:/dev/shm -e START_VNC_SERVER=true --user chrome --privileged jawisp/quaacr-doockr-chrome-93:0.0.17`

* if you are going to download files and would like to access them locally later, add this parameter:

    `-v /your/local/path:/home/chrome/Downloads`

    
* if you want to give it a name or run in detached mode, well, you know, just do it.


As you can see, there are some ports exposed:

* *5141* - for HTTP communication (start and stop web browser instance);

* *9999* - for TCP communication (booked for Devtools protocol messages );

* *5900* - VNC server, but you are already familiar with it, aren't you?

All these can be changed up to your needs via environment variables (see `Dockerfile`).