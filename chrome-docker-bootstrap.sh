#!/bin/bash

# Based on: http://www.richud.com/wiki/Ubuntu_Fluxbox_GUI_with_x11vnc_and_Xvfb

readonly G_LOG_I='[INFO]'
readonly G_LOG_W='[WARN]'
readonly G_LOG_E='[ERROR]'

main() {
    launch_xvfb
    launch_window_manager
    launch_tcp_proxy

    if [ "${START_VNC_SERVER}" = true ] ; then
      launch_vnc_server
    fi

    wait $!
}

launch_xvfb() {
    export GEOMETRY="${SCREEN_WIDTH}""x""${SCREEN_HEIGHT}""x""${SCREEN_DEPTH}"
    rm -f /tmp/.X*lock

    # Start and wait for either Xvfb to be fully up or we hit the timeout.
    /usr/bin/Xvfb "${DISPLAY}" -screen 0 "${GEOMETRY}" -dpi "${SCREEN_DPI}" -ac +extension RANDR &

    local count=0
    local timeout=${XVFB_TIMEOUT:-5}

    until xdpyinfo -display "${DISPLAY}" > /dev/null 2>&1
    do
        count=$((count+1))
        sleep 1
        if [ ${count} -gt "${timeout}" ]
        then
            echo "${G_LOG_E} xvfb failed to start."
            exit 1
        fi
    done
}

launch_window_manager() {
    # Start and wait for either fluxbox to be fully up or we hit the timeout.
    fluxbox -display "${DISPLAY}" &

    local count=0
    local timeout=${XVFB_TIMEOUT:-5}

    until wmctrl -m > /dev/null 2>&1
    do
        count=$((count+1))
        sleep 1
        if [ ${count} -gt "${timeout}" ]
        then
            echo "${G_LOG_E} fluxbox failed to start."
            exit 1
        fi
    done
}

launch_tcp_proxy() {
  java -jar "${HOME}"/quaacr-tcp-proxy/target/quaacr-tcp-proxy-0.17.jar &
}

launch_vnc_server() {
    local passwordArgument='-nopw'

    if [ -n "${VNC_SERVER_PASSWORD}" ]
    then
        local passwordFilePath="${HOME}/x11vnc.pass"
        if ! x11vnc -storepasswd "${VNC_SERVER_PASSWORD}" "${passwordFilePath}"
        then
            echo "${G_LOG_E} Failed to store x11vnc password."
            exit 1
        fi
        passwordArgument=-"-rfbauth ${passwordFilePath}"
        echo "${G_LOG_I} VNC server will ask for a password."
    else
        echo "${G_LOG_W} VNC server will NOT ask for a password."
    fi

    x11vnc -display "${DISPLAY}" -forever "${passwordArgument}" &
}

control_c() {
    echo ""
    exit
}

trap control_c SIGINT SIGTERM SIGHUP

main

exit
