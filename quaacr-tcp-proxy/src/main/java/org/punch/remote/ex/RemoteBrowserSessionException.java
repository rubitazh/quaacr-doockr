package org.punch.remote.ex;

public class RemoteBrowserSessionException extends RuntimeException {

    public RemoteBrowserSessionException() {
        super();
    }

    public RemoteBrowserSessionException(String msg) {
        super(msg);
    }

    public RemoteBrowserSessionException(String msg, Throwable e) {
        super(msg, e);
    }
}