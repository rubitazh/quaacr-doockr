/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.remote.http;

import org.punch.remote.ex.RemoteBrowserSessionException;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RemoteSession {

    private static final Logger logger = Logger.getLogger(RemoteSession.class.getName());

    // OS name quaacr is currently running on
    private static final String OS_NAME = System.getProperty("os.name");

    // Linux
    private static final String OS_LINUX = "Linux";

    // WS schema suffix to identify a browser debugging ws address in logs
    private static final String WS_SCHEMA = "ws://";

    /* Env variables */

    private static final String PATH_TO_EXEC = System.getenv("PATH_TO_EXEC");
    private static final String PROFILE_PATH = System.getenv("PROFILE_PATH");

    /* Default page settings */

    private static final String HOMEPAGE = "-homepage";
    private static final String ABOUT_BLANK = "about:blank";

    /* Chrome options */

    private static final String DEBUGGING_PORT = "--remote-debugging-port=0";
    private static final String DISABLE_DEFAULT_APPS = "--disable-default-apps";
    private static final String DISABLE_TRANSLATE = "--disable-features=Translate";
    private static final String ENABLE_AUTOMATION = "--enable-automation";
    private static final String NO_DEFAULT_BROWSER_CHECK = "--no-default-browser-check";
    private static final String NO_FIRST_RUN = "--no-first-run";
    private static final String USER_DATA_DIR_TEMPLATE = "--user-data-dir=%s";
    private static final String WINDOW_SIZE_TEMPLATE = "--window-size=%d,%d";

    /* Session param keys */

    private static final String NO_DEFAULT_OPTS = "noDefaultOpts";
    private static final String PROFILE_NAME = "profileName";
    private static final String SESSION_LOAD_TIMEOUT = "sessionLoadTimeout";
    private static final String WINDOW_SIZE = "windowSize";
    private static final String WINDOW_SIZE_RATIO = "windowSizeRatio";

    // Browser user data dirs
    private static final List<Path> dirLst = new CopyOnWriteArrayList<>();

    /* Removes profiles root dir on JVM shutdown. */
    static {
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            Path profilePath = Paths.get(PROFILE_PATH);
            FileUtils.deleteDirectory(profilePath);
        }));
    }

    private final AtomicBoolean isTerminated = new AtomicBoolean(false);
    private final Process process;
    private final Path usrDataDir;
    private final String wsUrl;

    private RemoteSession(Process process, Path usrDataDir, String wsUrl) {
        this.process = process;
        this.usrDataDir = usrDataDir;
        this.wsUrl = wsUrl;
    }

    public String getWsUrl() {
        return wsUrl;
    }

    public void destroy() {
        //Set terminate signal:
        if (!isTerminated.get()) {
            isTerminated.set(true);
        }
        //Close browser session:
        try {
            process.destroy();
            process.waitFor();
            logger.log(Level.INFO, "Browser closed.");

        } catch (InterruptedException e) {
            logger.log(Level.SEVERE, "Something went wrong on process kill. You know, it happens with all of us. " +
                    "[pid={0}]", process.pid());

        } finally {
            FileUtils.deleteDirectory(usrDataDir);
        }
    }

    public boolean isTerminated() {
        return isTerminated.get();
    }

    public static RemoteSession newInstance(List<String> browserOpts, Map<String, Object> sessionParams) {
        if (Objects.isNull(PATH_TO_EXEC)) {
            throw new NullPointerException("Path to Chrome not set.");
        }

        if (!OS_NAME.equals(OS_LINUX)) {
            throw new RuntimeException("How come it is running on non-Linux OS?");
        }

        //Here we try to start Chrome with all the staff we have here:
        List<String> args = new ArrayList<>();

        //Add path to Chrome executable: it MUST be first:
        args.add(PATH_TO_EXEC);

        //Open empty page:
        args.addAll(List.of(HOMEPAGE, ABOUT_BLANK));

        boolean noDefaultOpts = Boolean.parseBoolean((String) sessionParams.get(NO_DEFAULT_OPTS));
        //Default list of browser start parameters:
        if (!noDefaultOpts) {
            args.addAll(List.of(
                    DEBUGGING_PORT,
                    DISABLE_DEFAULT_APPS,
                    DISABLE_TRANSLATE,
                    ENABLE_AUTOMATION,
                    NO_FIRST_RUN,
                    NO_DEFAULT_BROWSER_CHECK));
        }

        if (Objects.nonNull(browserOpts)) {
            args.addAll(browserOpts);
        }

        //Set browser profile:
        Path usrDataDir = Paths.get(PROFILE_PATH, (String) sessionParams.get(PROFILE_NAME));

        if (dirLst.contains(usrDataDir)) {
            throw new RemoteBrowserSessionException("It's not allowed to start new browser session with existing user data. " +
                    "[userdata =" + usrDataDir + "]");
        }

        args.add(String.format(USER_DATA_DIR_TEMPLATE, usrDataDir));

        //Setup browser window size:
        String userDefinedWindowSizeStr = (String) sessionParams.get(WINDOW_SIZE);
        int width, height;
        if (userDefinedWindowSizeStr == null) {
            Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
            width = screen.width;
            height = screen.height;

        } else {
            String[] userDefinedWindowSizeArr = userDefinedWindowSizeStr.split("x");
            width = Integer.parseInt(userDefinedWindowSizeArr[0]);
            height = Integer.parseInt(userDefinedWindowSizeArr[1]);
        }

        float ratio = Float.parseFloat(sessionParams.get(WINDOW_SIZE_RATIO).toString());
        String windowSize = String.format(WINDOW_SIZE_TEMPLATE, (int) (width * ratio), (int) (height * ratio));
        args.add(windowSize);

        //Init browser process:
        ProcessBuilder pb = new ProcessBuilder();
        pb.command(args);
        Process p;
        try {
            p = pb.start();
            //Collect dir names:
            dirLst.add(usrDataDir);

        } catch (IOException e) {
            throw new RemoteBrowserSessionException("Cannot start browser process.", e);
        }

        CompletableFuture<RemoteSession> future = new CompletableFuture<>();
        Thread t = new RemoteSession.ChromeRunner(p, usrDataDir, future);
        t.start();

        try {
            long sessionLoadTimeout = Long.parseLong(sessionParams.get(SESSION_LOAD_TIMEOUT).toString());
            return future.get(sessionLoadTimeout, TimeUnit.MILLISECONDS);

        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            t.interrupt();
            ProcessHandle.of(p.pid()).ifPresent(ProcessHandle::destroyForcibly);
            FileUtils.deleteDirectory(usrDataDir);

            throw new RemoteBrowserSessionException("Browser failed to start. [pid=" + p.pid() + "]", e);
        }
    }

    private static class ChromeRunner extends Thread {

        private final Process process;
        private final Path usrDataDir;
        private final CompletableFuture<RemoteSession> future;

        ChromeRunner(Process process, Path usrDataDir, CompletableFuture<RemoteSession> future) {
            this.process = process;
            this.usrDataDir = usrDataDir;
            this.future = future;
        }

        @Override
        public void run() {
            try (var es = process.getErrorStream();
                 var esr = new InputStreamReader(es);
                 var ebr = new BufferedReader(esr)) {

                String wsUrl;
                String esLine;
                List<String> esLines = null;
                while (Objects.nonNull(esLine = ebr.readLine())) {
                    if (!esLine.isBlank()) {
                        esLines = Objects.isNull(esLines) ? new ArrayList<>() : esLines;
                        esLines.add(esLine);
                        logger.log(Level.INFO, esLine);
                    }

                    if (esLine.contains(WS_SCHEMA)) {
                        wsUrl = esLine.substring(esLine.indexOf(WS_SCHEMA));
                        future.complete(new RemoteSession(process, usrDataDir, wsUrl));
                        return;
                    }
                }

                final String EXC_MSG;
                if (Objects.isNull(esLines)) {
                    EXC_MSG = "Remote browser failed to start.";

                } else {
                    StringBuilder errBuilder = new StringBuilder();
                    for (String line : esLines) {
                        errBuilder.append("error: ")
                                .append(line)
                                .append("\n");
                    }

                    String errBuilderStr = errBuilder.toString();
                    EXC_MSG = errBuilderStr.substring(0, errBuilderStr.length() - 1);
                }

                future.completeExceptionally(new RuntimeException(EXC_MSG));
                logger.log(Level.SEVERE, "Browser failed to start. See log errors, if any. [pid={0}]", process.pid());

                try (var is = process.getInputStream();
                     var isr = new InputStreamReader(is);
                     var br = new BufferedReader(isr)) {

                    String isLog;
                    while (Objects.nonNull(isLog = br.readLine())) {
                        logger.log(Level.INFO, isLog);
                    }
                }

            } catch (IOException e) {
                logger.log(Level.SEVERE, "Console log error.", e);
            }
        }
    }
}
