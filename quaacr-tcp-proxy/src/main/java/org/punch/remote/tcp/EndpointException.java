/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.remote.tcp;

public class EndpointException extends Exception {

    public EndpointException(String msg) {
        super(msg);
    }
}
