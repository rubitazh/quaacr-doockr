/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.remote.http;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.punch.remote.tcp.ProxyServer;
import org.punch.remote.tcp.RemoteWsEndpoint;
import org.punch.remote.utils.Utils;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ChromeSessionController {

    private static final Logger logger = Logger.getLogger(ChromeSessionController.class.getName());

    /* Endpoints */
    private static final String GET = "/chrome/get";
    private static final String CLOSE = "/chrome/close";
    private static final String CLEANUP = "/chrome/cleanup";
    private static final String COMMAND = "/remote/command";

    private final Map<String, RemoteSession> sessions = new ConcurrentHashMap<>();
    private final int port;
    private final ProxyServer proxy;
    private final ExecutorService exec;

    public ChromeSessionController(int port, ProxyServer proxy, ExecutorService exec) {
        this.port = port;
        this.proxy = proxy;
        this.exec = exec;
    }

    public void start() throws IOException {
        InetSocketAddress addr = new InetSocketAddress(port);
        HttpServer server = HttpServer.create(addr, 0);
        server.createContext(GET, new GetChromeHandler(sessions, proxy));
        server.createContext(CLOSE, new CloseChromeHandler(sessions));
        server.createContext(CLEANUP, new CleanupChromeHandler());
        server.createContext(COMMAND, new CommandExecutionHandler());
        server.setExecutor(exec);
        server.start();

        logger.log(Level.INFO, "Http server started. [port={0,number,#}]", port);
    }

    public void forceClose(String id) {
        RemoteSession session = sessions.get(id);

        if (Objects.isNull(session)) {
            logger.log(Level.FINEST, "Session not exist anymore. [id={0}]", id);

            return;
        }

        session.destroy();
        sessions.remove(id);

        logger.log(Level.WARNING, "Session closed forcibly. [id={0}]", id);
    }

    public boolean isSessionTerminated(String id) {
        RemoteSession session = sessions.get(id);
        return Objects.isNull(session) || session.isTerminated();
    }

    private static class GetChromeHandler implements HttpHandler {

        private final Map<String, RemoteSession> sessions;
        private final ProxyServer proxy;

        public GetChromeHandler(Map<String, RemoteSession> sessions, ProxyServer proxy) {
            this.sessions = sessions;
            this.proxy = proxy;
        }

        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String query = exchange.getRequestURI().getQuery();

            if (Objects.isNull(query)) {
                final String NO_PARAMS_SET = "No session params set.";

                logger.log(Level.SEVERE, NO_PARAMS_SET);

                sendResponse(exchange, 400, NO_PARAMS_SET);
                return;
            }

            //Parse browser opts:
            final String BROWSER_OPTS = "browserOpts=";
            List<String> browserOpts = null;
            if (query.contains(BROWSER_OPTS)) {
                Pattern optsPattern = Pattern.compile(BROWSER_OPTS + "[A-Za-z0-9-=,.:/]+");
                Matcher optsMatcher = optsPattern.matcher(query);

                StringBuilder optsBuilder = new StringBuilder();
                while (optsMatcher.find()) {
                    optsBuilder.append(optsMatcher.group());
                }

                String optsMatches = optsBuilder.toString();
                if (optsMatches.isEmpty()) {
                    final String CANNOT_PARSE_BROWSER_OPTS = "Cannot parse browser opts.";

                    logger.log(Level.SEVERE, CANNOT_PARSE_BROWSER_OPTS);

                    sendResponse(exchange, 500, CANNOT_PARSE_BROWSER_OPTS);
                    return;
                }

                browserOpts = List.of(optsMatches.replace(BROWSER_OPTS, "").split(","));

                logger.log(Level.INFO, "Browser options received. [opts={0}]",
                        browserOpts.toString().replace("[", "").replace("]", ""));
            }

            //Parse session params:
            Pattern paramsPattern = Pattern.compile("&\\w+=\\S+");
            Matcher paramsMatcher = paramsPattern.matcher(query);

            StringBuilder paramsBuilder = new StringBuilder();
            while (paramsMatcher.find()) {
                paramsBuilder.append(paramsMatcher.group());
            }

            String paramsMatches = paramsBuilder.toString();

            if (paramsMatches.isEmpty()) {
                final String CANNOT_PARSE_SESSION_PARAMS = "Cannot parse session params.";

                logger.log(Level.SEVERE, CANNOT_PARSE_SESSION_PARAMS);

                sendResponse(exchange, 500, CANNOT_PARSE_SESSION_PARAMS);
                return;
            }

            //Here we need try-catch block for:
            // 1) query param string parsing failure;
            // 2) new remote session instance failure
            // 3) endpoint connection failure.
            RemoteSession remoteSession = null;
            String wsUrl;
            try {
                Map<String, Object> sessionParams = Arrays.stream(paramsMatches.split("&"))
                        .filter(s -> !s.isEmpty())
                        .map(s -> s.split("="))
                        .collect(Collectors.toMap(pair -> pair[0], pair -> pair[1]));

                logger.log(Level.INFO, "Session params received. [params={0}]",
                        sessionParams.toString().replace("{", "").replace("}", ""));

                remoteSession = RemoteSession.newInstance(browserOpts, sessionParams);
                wsUrl = remoteSession.getWsUrl();
                RemoteWsEndpoint endpoint = new RemoteWsEndpoint(proxy, wsUrl);
                endpoint.connect();

            } catch (Exception e) {
                sendResponse(exchange, 500, toExcString(e, new StringBuilder()));

                if (Objects.nonNull(remoteSession)) {
                    remoteSession.destroy();
                }

                return;
            }

            //Send Chrome debug endpoint:
            sessions.putIfAbsent(Utils.getId(wsUrl), remoteSession);
            sendResponse(exchange, 200, wsUrl);
        }
    }

    private static class CloseChromeHandler implements HttpHandler {

        private Map<String, RemoteSession> sessions;

        public CloseChromeHandler(Map<String, RemoteSession> sessions) {
            this.sessions = sessions;
        }

        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String q = exchange.getRequestURI().getQuery();
            String id = q.split("=")[1];

            if (Objects.isNull(id) || id.isEmpty()) {
                String error = "Invalid request query. [q=" + q + "]";

                logger.log(Level.SEVERE, error);

                sendResponse(exchange, 400, error);
                return;
            }

            if (!sessions.containsKey(id)) {
                String msg = "Session already closed. [id=" + id + "]";

                logger.log(Level.INFO, msg);

                sendResponse(exchange, 301, msg);
                return;
            }

            try {
                sessions.get(id).destroy();
                sendResponse(exchange, 200, id);

            } catch (Exception e) {
                sendResponse(exchange, 500, toExcString(e, new StringBuilder()));
            }
        }
    }

    private static class CleanupChromeHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange exchange) throws IOException {
            FileUtils.deleteDirectory(Paths.get(System.getenv("PROFILE_PATH")));

            logger.log(Level.INFO, "Profile data removed.");

            sendResponse(exchange, 200, "true");
        }
    }

    private static class CommandExecutionHandler implements HttpHandler {

        @Override
        public void handle(HttpExchange exchange) throws IOException {
            String q = exchange.getRequestURI().getQuery();

            if (Objects.isNull(q)) {
                final String COMMAND_NOT_SPECIFIED = "Remote command not specified.";

                logger.log(Level.SEVERE, COMMAND_NOT_SPECIFIED);

                sendResponse(exchange, 400, COMMAND_NOT_SPECIFIED);
                return;
            }

            String cmd = URLDecoder.decode(q, StandardCharsets.UTF_8);
            ProcessBuilder pb = new ProcessBuilder("/bin/sh", "-c", cmd);
            Process process = pb.start();

            try (Reader istrReader = new InputStreamReader(new BufferedInputStream(process.getInputStream()))) {
                StringBuilder istrBuilder = new StringBuilder();
                int istrCh;

                while ((istrCh = istrReader.read()) != -1) {
                    istrBuilder.append((char) istrCh);
                }

                String istrOutput = istrBuilder.toString();

                if (istrOutput.isEmpty()) {
                    try (Reader errReader = new InputStreamReader(new BufferedInputStream(process.getErrorStream()))) {
                        StringBuilder errBuilder = new StringBuilder();
                        int errCh;

                        while ((errCh = errReader.read()) != -1) {
                            errBuilder.append((char) errCh);
                        }

                        sendResponse(exchange, 200, errBuilder.toString());
                    }

                } else {
                    sendResponse(exchange, 200, istrOutput);
                }
            }
        }
    }

    private static void sendResponse(HttpExchange exchange, int code, String msg) throws IOException {
        exchange.sendResponseHeaders(code, msg.length());
        OutputStream os = exchange.getResponseBody();
        os.write(msg.getBytes(StandardCharsets.UTF_8));
        os.flush();
        os.close();
        exchange.close();
    }

    private static String toExcString(Throwable e, StringBuilder sb) {
        if (Objects.isNull(e)) {
            return sb.toString();
        }

        if (sb.length() != 0) {
            sb.append("\nCaused by ");
        }

        sb.append(e.toString());
        Arrays.stream(e.getStackTrace()).forEach(ste -> sb.append("\n\t").append(ste));
        return toExcString(e.getCause(), sb);
    }
}