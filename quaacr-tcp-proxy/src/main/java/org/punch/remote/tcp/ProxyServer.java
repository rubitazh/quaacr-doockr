/*
 * Copyright (c) 2020. Dmitriy A. Yakovlev
 *
 * This file is part of quaacr project which is released under MIT license.
 * See file LICENSE.md or go to https://mit-license.org/ for full license details.
 */

package org.punch.remote.tcp;

import org.punch.remote.http.ChromeSessionController;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProxyServer {

    private static final Logger logger = Logger.getLogger(ProxyServer.class.getName());

    private static final long AWAIT_CONN_TIMEOUT = Long.parseLong(System.getenv("AWAIT_CONN_TIMEOUT"));
    private static final int N_LEN_BYTES = 4;
    private static final int N_ID_BYTES = 36;

    /* Registered active connections */
    private final Map<String, ConnectionData> connections = new ConcurrentHashMap<>();

    /* Connections, that were closed unexpectedly (for logging only) */
    private final List<String> closedForcibly = new ArrayList<>();

    private final int port;
    private final ExecutorService exec;

    private ServerSocket server;
    private ChromeSessionController controller;

    public ProxyServer(int port, ExecutorService exec) {
        this.port = port;
        this.exec = exec;
    }

    public void registerController(ChromeSessionController controller) {
        this.controller = controller;
    }

    public void start() throws IOException {
        try {
            server = new ServerSocket(port);

            logger.log(Level.INFO, "TCP proxy started. [port={0,number,#}]", port);

            while (!exec.isShutdown()) {
                try {
                    final Socket client = server.accept();
                    exec.execute(() -> handle(client));

                } catch (RejectedExecutionException e) {

                    if (!exec.isShutdown()) {
                        logger.log(Level.WARNING, "Connection rejected.");
                    }
                }
            }

        } catch (IOException e) {
            if (server.isClosed()) {
                logger.log(Level.INFO, "TCP proxy already closed.");
                return;
            }

            throw new IOException("TCP proxy error.", e);
        }
    }

    //TODO: Not needed?
    public void stop() throws InterruptedException, IOException {
        logger.log(Level.FINE, "Forcibly closed connections. [connections={0}]", closedForcibly.toString());

        exec.shutdown();
        exec.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);

        logger.log(Level.INFO, "All connections terminated.");

        server.close();

        logger.log(Level.INFO, "Closing TCP proxy...");
    }

    void accept(String id, String txt) throws EndpointException {
        ConnectionData data = getConnectionData(id, "accept");

        if (Objects.isNull(data)) {
            if (controller.isSessionTerminated(id)) {
                return;
            }

            if (closedForcibly.contains(id)) {
                return;
            }

            logger.log(Level.WARNING, "Connection with client lost - closing Chrome session... [id={0}]", id);

            controller.forceClose(id);
            closedForcibly.add(id);

            return;
        }

        if (Objects.isNull(data.socket)) {

            if (data.endpoint == null) {
                throw new EndpointException("Endpoint not registered. [id=" + id + "]");
            }

            long start = System.currentTimeMillis();

            for (;;) {
                if ((System.currentTimeMillis() - start) > AWAIT_CONN_TIMEOUT) {
                    throw new EndpointException("Connection socket not set. [id=" + id + "]");
                }

                if (Objects.nonNull(data.socket)) {
                    break;
                }
            }
        }

        try {
            OutputStream os = new BufferedOutputStream(data.socket.getOutputStream());
            //write length:
            byte[] tbf = txt.getBytes(StandardCharsets.UTF_8);
            byte[] lbf = ByteBuffer.allocate(N_LEN_BYTES).putInt(tbf.length).array();
            os.write(lbf);
            //write id:
            os.write(id.getBytes(StandardCharsets.UTF_8));
            //write message:
            os.write(tbf);
            os.flush();

        } catch (IOException e) {
            logger.log(Level.INFO, "End of the stream. [id={0}]", id);
            //Close Chrome session:
            controller.forceClose(id);
        }
    }

    void registerEndpoint(String endpointId, WsEndpoint endpoint) {
        connections.putIfAbsent(endpointId, new ConnectionData(endpoint));
    }

    /**
     * The rule is:
     * 1) first 4 bytes are the message length;
     * 2) next 36 bytes are the ws endpoint id;
     * 3) rest of the bytes are the message body.
     */
    private void handle(Socket socket) {
        InputStream is;
        try {
            is = new BufferedInputStream(socket.getInputStream());

        } catch (IOException e) {
            handleConnEx(socket, e);
            return;
        }

        byte[] lbf, ibf, mbf;
        boolean isRegistered = false;
        String id = null;

        while (true) {
            try {
                //read length:
                lbf = read(is, N_LEN_BYTES);
                int len = ByteBuffer.wrap(lbf).getInt();

                //read endpoint id:
                ibf = read(is, N_ID_BYTES);
                id = new String(ibf, StandardCharsets.UTF_8);
                var conn = getConnectionData(id, "handle");

                if (conn == null) {
                    logger.log(Level.SEVERE, "Somehow we lost connection that must exist. " +
                            "Man, I think it's time for some hard core debugging. [id={0}]", id);

                    break;
                }

                //Register new connection:
                if (!isRegistered) {
                    conn.registerSocket(socket);
                    isRegistered = true;
                }

                //read message:
                mbf = read(is, len);
                var msg = new String(mbf, StandardCharsets.UTF_8);
                //Send message to WsEndpoint:
                conn.endpoint.sendMsg(msg);

            } catch (IOException e) {
                logger.log(Level.INFO, "Proxy listener stopped.");

                if (Objects.isNull(id)) {
                    logger.log(Level.SEVERE, "Endpoint id not set.");
                    return;
                }

                removeConnection(id);
                break;
            }
        }
        try {
            is.close();

        } catch (IOException e) {
            consumeExOnClose(e);
        }
    }

    private byte[] read(InputStream is, int len) throws IOException {
        byte[] bf = new byte[len];
        int offset = 0;

        while (true) {
            int read = is.read(bf, offset, len - offset);

            if (read == -1) {
                throw new IOException("End of the stream.");
            }

            if (read > 0) {
                if (offset + read == len) return bf;

                offset += read;
            }
        }
    }

    private void handleConnEx(Socket conn, Throwable e) {
        if (conn.isClosed()) {
            logger.log(Level.INFO, "Connection closed.");
            return;
        }

        logger.log(Level.WARNING, "Stream terminated - closing connection.", e);

        try {
            conn.close();

        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Close connection error.", e);
        }
    }

    private void consumeExOnClose(Throwable e) {
        logger.log(Level.SEVERE, "Close connection error.", e);
    }

    private ConnectionData getConnectionData(String id, String mtd) {
        var connection = connections.get(id);

        if (Objects.isNull(connection)) {
            logger.log(Level.FINE, "Connection already removed. [id={0}, method={1}]", new Object[]{id, mtd});

            return null;
        }

        return connection;
    }

    private void removeConnection(String id) {
        connections.remove(id);

        logger.log(Level.INFO, "Connection removed. [id={0}]", id);
    }

    private static class ConnectionData {

        private final WsEndpoint endpoint;
        private Socket socket;

        private ConnectionData(WsEndpoint endpoint) {
            this.endpoint = endpoint;

        }

        void registerSocket(Socket socket) {
            if (Objects.isNull(this.socket)) {
                this.socket = socket;
            }
        }
    }
}
